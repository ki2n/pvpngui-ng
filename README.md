# pvpngui-ng
A fork of ProtonVPN-GTK that uses Python 3 and some other improvements

# Features
- Server Connect (Fastest, Random)
- protonvpn-cli Updater
- Live status of IP Address, Location & Connection Status

**Please remember this is a work in progress in very early stages of development.**

![alt text](https://i.imgur.com/JaM7sCu.png "ProtonVPN-GTK Screenshot")

# Dependencies
- [protonvpn-cli](https://github.com/ProtonVPN/linux-cli.git)
- Python 3.x
  - requests
  - json
  - schedule
  - pygobject
  - configparser

# Installation Instructions
#### 1. Initialize ProtonVPN-CLI (Prerequisite)

If you already have completed this step, then you can skip to Step 2.
Otherwise, please run `protonvpn-cli --init` in the terminal program of your choice and follow 
the onscreen prompts.

#### 2. Clone repository

`git clone https://gitlab.com/ki2n/pvpngui-ng.git`

#### 3. TBA


# Current Issues
- Working on making a standalone executable
- ~~No installable package would like to use Flatpak~~
- ~~TCP/UDP Selection missing~~
- ~~Server List incomplete~~
- ~~Server list not filtered for free/paid users~~

# Report Bugs
I'm sure there's plenty of bugs from the original project (and possibly ones that I'll make as well as the project develops!).
If you do find any, please feel free to report them here. Or even better, have a go at fixing one!

# Contribute
Contributions more than welcome!
